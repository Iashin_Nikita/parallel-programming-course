#include "cuda_runtime.h"
#include <stdio.h>

#include <math_functions.h>
#define _USE_MATH_DEFINES
#include <math.h>

 #include <cuda.h> 
 #include <device_launch_parameters.h> 
 #include <texture_fetch_functions.h> 
 #include <builtin_types.h> 
 #include <vector_functions.h> 
 #include "float.h"

__constant__ int width;			// image's width in BYTES
__constant__ int widthStep;
__constant__ int height;
__constant__ int heightStep;
__constant__ float gammaCorRatio;

__global__ void kernel_gamma( unsigned char *data)
{
	int idX = threadIdx.x % 32;
	int idY = threadIdx.x / 32;
	int tmpx;
	int tmpy;
	float tmp;

	for (int i = 0; i < widthStep; i++)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		for (int j = 0; j < heightStep; j++)
		{
			tmpy = j + idY * heightStep;
			if ( tmpy > height)
				break;

			// important
			tmp =  pow((float)data[tmpy * width + tmpx], gammaCorRatio);
			if (tmp > 255)
				data[tmpy * width + tmpx] = 255;
			else
				data[tmpy * width + tmpx] = tmp;
		}
	}
}

__global__ void kernel_grayscale( unsigned char *data, unsigned char *output)
{
	int idX = threadIdx.x % 32;
	int idY = threadIdx.x / 32;
	int tmpx;
	int tmpy;

	for (int i = 0; i < widthStep; i = i + 3)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		for (int j = 0; j < heightStep; j++)
		{
			tmpy = j + idY * heightStep;
			if ( tmpy > height)
				break;

			//important
			output[tmpy * width / 3 + tmpx / 3] = (data[tmpy * width + tmpx] + data[tmpy * width + tmpx + 1] + data[tmpy * width + tmpx + 2])/3;
		}
	}
}

__global__ void kernel_visualizeProcessingBlocks(unsigned char *output)
{
	// temporary
	// should adjust it according to blocks and grid
	int idX = threadIdx.x % 32; // blocks X index
	int idY = threadIdx.x / 32; // blocks Y index
	int tmpx;					// blocks first pixel X index
	int tmpy;					// blocks first pixel Y index

	for (int i = 0; i < widthStep; i++)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		if (i == 0 || i == widthStep -1)
		{
			for (int j = 0; j < heightStep; j++)
			{

				tmpy = j + idY * heightStep;
				if ( tmpy > height)
					break;

				//important
				int baseoffset = tmpy * width + tmpx;
				output[baseoffset] = 255;
			}
		}
		else
		{
			for (int j = 0; j < heightStep; j += heightStep - 1)
			{
				tmpy = j + idY * heightStep;
				if ( tmpy > height)
					break;
			
				//important
				int baseoffset = tmpy * width + tmpx;
				output[baseoffset] = 255;
			}
		}
	}
}

__constant__ float gaussMask[5][5] = {
    { 2, 4, 5, 4, 2 },
	{ 4, 9, 12, 9, 4 },
	{ 5, 12, 15, 12, 5 },
	{ 4, 9, 12, 9, 4 },
    { 2, 4, 5, 4, 2 },
};

__global__ void kernel_gaussOverGray( unsigned char *data, unsigned char *output)
{
	int idX = threadIdx.x % 32; // blocks X index
	int idY = threadIdx.x / 32; // blocks Y index
	int tmpx;					// blocks first pixel X index
	int tmpy;					// blocks first pixel Y index
	int initI, endI;			// used to avoid blurring first/last two rows and columns
	int initJ, endJ;
	float accumulator;
	/* weird index stuff that is somehow unnessesary(cuda handles index-out-of-bounds automatically?)
	if (idX == 0) initI = 2;
	else initI = 0;
	if (idX == 31) endI = (width - (widthStep * 31) - 2);
	else endI = widthStep;

	if (idY == 0) initJ = 2;
	else initJ = 0;
	if (idY == 31) endJ = (height - (heightStep * 31) - 2);
	else endJ = heightStep;*/
	initI = 0;
	endI = widthStep;			
	initJ = 0;
	endJ = heightStep;
	
	for (int i = initI; i < endI; i++)
	{
		tmpx = i + idX * widthStep;

		for (int j = initJ; j < endJ; j++)
		{
			tmpy = j + idY * heightStep;
			if ( tmpy > height)
				break;
			
			//important
			accumulator = 0;
			// offset to the current pixel in data
			int baseoffset = tmpy * width + tmpx;
			for ( int indexX = -2; indexX < 3;indexX ++)
			{
				for ( int indexY = -2; indexY < 3;indexY ++)
				{
					accumulator += data[baseoffset + indexY * width + indexX] * gaussMask[2 + indexX][2 + indexY];
				}
			}
			output[baseoffset] = accumulator / 159;
		}
	}

	// handling borders seperately
	// moi mozg bolit, dodelat' pozje
	/*
	bool flag = false;
	if (initI == 2)
	{
		initI = 0;
		endI = 3;
		flag - true;
	}
	else if ( endI != widthStep)
	{
		endI = widthStep;
		initI = widthStep - 2;
		flag = true;
	}

	if (initJ == 2)
	{
		initJ = 0;
		endJ = 3;
		flag = true;
	}
	else if (endJ == heightStep - 2)
	{
		endJ = heightStep;
		initJ = heightStep - 2;
		flag = true;
	}
	
	if (flag)
		for (int i = initI; i < endI; i++)
		{
			tmpx = i + idX * widthStep;
			if (tmpx > width)
				break;
			for (int j = initJ; j < endJ; j++)
			{
				tmpy = j + idY * heightStep;
				if ( tmpy > height)
					break;
				output[tmpy * width + tmpx] = data[tmpy * width + tmpx];
				//output[tmpy * width + tmpx] = 0;
			}
		}*/
}

__constant__ float sobelMask_horizontal[3][3] = {
	{ -1, 0, 1},
	{ -2, 0, 2},
	{ -1, 0, 1}
};
__constant__ float sobelMask_vertical[3][3] = {
	{ -1, -2, -1},
	{ 0, 0, 0},
	{ 1, 2, 1}
};
__constant__ float sharMask_horizontal[3][3] = {
	{ -3, 0, 3},
	{ -10, 0, 10},
	{ -3, 0, 3}
};
__constant__ float sharMask_vertical[3][3] = {
	{ -3, -10, -3},
	{ 0, 0, 0},
	{ 3, 10, 3}
};
__constant__ int sobelFlag;
__global__ void kernel_EdgeDetection( unsigned char *data, unsigned char *output, float *dirOutput)
{
	// if flag == true - Sobel matrix
	// if flag == false - Sharr matrix
		// temporary
	// should adjust it according to blocks and grid
	int idX = threadIdx.x % 32; // blocks X index
	int idY = threadIdx.x / 32; // blocks Y index
	int tmpx;					// blocks first pixel X index
	int tmpy;					// blocks first pixel Y index
	
	float accumulator_horizontal = 0;
	float accumulator_vertical = 0;

	int initI, endI;			// used to avoid blurring first/last two rows and columns
	int initJ, endJ;
	initI = 0;
	endI = widthStep;			
	initJ = 0;
	endJ = heightStep;
	
	for (int i = initI; i <= endI; i++)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		for (int j = initJ; j <= endJ; j++)
		{
			tmpy = j + idY * heightStep;
			if ( tmpy > height)
				break;
			
			//important
			accumulator_horizontal = 0;
			accumulator_vertical = 0;
			// offset to the current pixel in data
			int baseoffset = tmpy * width + tmpx;

			if (sobelFlag==1)
			{
				for ( int indexX = -1; indexX < 2; indexX ++)
				{
					for ( int indexY = -1; indexY < 2;indexY ++)
					{
						accumulator_vertical += data[baseoffset + indexY * width + indexX] * sobelMask_vertical[1 + indexX][1 + indexY];
						accumulator_horizontal += data[baseoffset + indexY * width + indexX] * sobelMask_horizontal[1 + indexX][1 + indexY];
					}
				}
				accumulator_vertical /= 2;
				accumulator_horizontal /= 2;

				unsigned char tmp = static_cast<unsigned char>(sqrt(accumulator_vertical*accumulator_vertical + accumulator_horizontal*accumulator_horizontal));
				output[baseoffset] = tmp;

				if (tmp > 5)
					dirOutput[baseoffset] = atan2f(accumulator_vertical, accumulator_horizontal);
			}
			else
			{
				for ( int indexX = -1; indexX < 2; indexX ++)
				{
					for ( int indexY = -1; indexY < 2;indexY ++)
					{
						accumulator_vertical += data[baseoffset + indexY * width + indexX] * sharMask_vertical[1 + indexX][1 + indexY];
						accumulator_horizontal += data[baseoffset + indexY * width + indexX] * sharMask_horizontal[1 + indexX][1 + indexY];
					}
				}
				accumulator_vertical /= 8;
				accumulator_horizontal /= 8;

				unsigned char tmp = static_cast<unsigned char>(sqrt(accumulator_vertical*accumulator_vertical + accumulator_horizontal*accumulator_horizontal));
				output[baseoffset] = tmp;

				if (tmp > 5)
					dirOutput[baseoffset] = atan2f(accumulator_vertical, accumulator_horizontal);
			}
		}
	}
}

__global__ void kernel_radianToColor( float *data, float *output)
{
	// data - float[size]
	// output - unsigned char[size*3]

	int idX = threadIdx.x % 32; // blocks X index
	int idY = threadIdx.x / 32; // blocks Y index
	int tmpx;					// blocks first pixel X index
	int tmpy;					// blocks first pixel Y index
	
	float accumulator_horizontal = 0;
	float accumulator_vertical = 0;

	int initI, endI;			// used to avoid blurring first/last two rows and columns
	int initJ, endJ;
	initI = 0;
	endI = widthStep;			
	initJ = 0;
	endJ = heightStep;
	
	for (int i = initI; i <= endI; i++)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		for (int j = initJ; j <= endJ; j++)
		{
			tmpy = j + idY * heightStep;
			if ( tmpy > height)
				break;
			

			unsigned char tmp = static_cast<unsigned char>((data[tmpy * width + tmpx] + M_PI)*81);
            if (tmp < 0)
				tmp = 0;

			//output[(tmpy * width + tmpx) + tmp/86] = tmp * M_PI;
		}
	}
}