#include "cuda_runtime.h"
#include <stdio.h>
#include <math_functions.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <cuda.h> 
#include <device_launch_parameters.h> 
#include <texture_fetch_functions.h> 
#include <builtin_types.h> 
#include <vector_functions.h> 
#include "float.h"

/*		/////////////
		ADRESSING WITHIN A KERNEL
		/////////////

		Grid has (x,y) format
		Blocks have (x,y) format

		//////
		Adresses in DATA
		///
		Data offset of the block's first pixel:
			baseOffset_block = blockIdx.y * (block_height * width) + blockIdx.x * block_width

		Data offset of the thread's first pixel:
			baseOffset_thread = baseOffset_block + threadIdx.y*(width*thread_height) + threadIdx.x * thread_width

		Data offset of THREAD's (y,x) pixel:
			pixelOffset = baseOffset_thread + y*width + x     (or x*3 for rgb)

		//////
		Adresses in original image
		///
		Thread's first pixel row:
			pixel_row = blockIdx.y * block_height + threadIdx.y*thread_height

		Thread's first pixel column:
			pixel_column = blockIdx.x * block_width + threadIdx.x * thread_width

		Thread's custom pixel column:
			pixel_column = blockIdx.x * block_width + threadIdx.x * thread_width + x	(or x*3 for rgb)
		/////////////
*/

//////// Image vars
__constant__ int height;
__constant__ int width;			// in BYTES
//////// Grid vars
__constant__ int block_height;
__constant__ int block_width;	// in BYTES
//////// Block vars
__constant__ int thread_height;
__constant__ int thread_width;	// in BYTES

//temp
__constant__ int widthStep,heightStep;

//////// Gamma correction
// (bitrate independant)
__constant__ float gammaCorRatio;

__global__ void kernel_gamma( unsigned char *data)
{
	int baseOffset_thread = blockIdx.y*(block_height*width) + blockIdx.x*block_width + threadIdx.y*(width*thread_height) + threadIdx.x * thread_width;
	int pixelOffset;

	for (int y = 0; y < thread_height; y++)
	{
		// If the current row went out of image bounds - leave the cycle
		if (blockIdx.y * block_height + threadIdx.y*thread_height + y >= height)
			break;
		for (int x = 0; x < thread_width; x++)
		{
			// If the current column went out of bounds - proceed to next row
			if (blockIdx.x * block_width + threadIdx.x * thread_width + x >= width)
				break;
			
			// important
			pixelOffset = baseOffset_thread + y*width + x;
			float val = pow((float)data[pixelOffset], gammaCorRatio);
			if (val > 255)
				data[pixelOffset] = 255;
			else
				data[pixelOffset] = val;
		} 
	}
}

//////// Grayscale
// 24bpp to 8bpp
__global__ void kernel_grayscale( unsigned char *data, unsigned char *output)
{
	int baseOffset_thread = blockIdx.y*(block_height*width) + blockIdx.x*block_width + threadIdx.y*(width*thread_height) + threadIdx.x * thread_width;
	int pixelOffset;

	for (int y = 0; y < thread_height; y++)
	{
		// If the current row went out of image bounds - leave the cycle
		if (blockIdx.y * block_height + threadIdx.y*thread_height + y >= height)
			break;
		for (int x = 0; x < thread_width; x = x+3)
		{
			// If the current column went out of bounds - proceed to next row
			if (blockIdx.x * block_width + threadIdx.x * thread_width + x >= width)
				break;
			
			// important
			pixelOffset = baseOffset_thread + y*width + x;
			output[pixelOffset/3] = static_cast<unsigned char>((data[pixelOffset] + data[pixelOffset+1]+data[pixelOffset+2])/3);
		}
	}
}

//TODO: decide what to do with dis
__global__ void kernel_visualizeProcessingBlocks(unsigned char *output)
{
	// temporary
	// should adjust it according to blocks and grid
	int idX = threadIdx.x % 32; // blocks X index
	int idY = threadIdx.x / 32; // blocks Y index
	int tmpx;					// blocks first pixel X index
	int tmpy;					// blocks first pixel Y index

	for (int i = 0; i < widthStep; i++)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		if (i == 0 || i == widthStep -1)
		{
			for (int j = 0; j < heightStep; j++)
			{

				tmpy = j + idY * heightStep;
				if ( tmpy > height)
					break;

				//important
				int baseoffset = tmpy * width + tmpx;
				output[baseoffset] = 255;
			}
		}
		else
		{
			for (int j = 0; j < heightStep; j += heightStep - 1)
			{
				tmpy = j + idY * heightStep;
				if ( tmpy > height)
					break;
			
				//important
				int baseoffset = tmpy * width + tmpx;
				output[baseoffset] = 255;
			}
		}
	}
}

//////// Gauss blur
// 8bpp
__constant__ float gaussMask[5][5] = {
    { 2, 4, 5, 4, 2 },
	{ 4, 9, 12, 9, 4 },
	{ 5, 12, 15, 12, 5 },
	{ 4, 9, 12, 9, 4 },
    { 2, 4, 5, 4, 2 },
};

__global__ void kernel_gaussOverGray( unsigned char *data, unsigned char *output)
{
	int baseOffset_thread = blockIdx.y*(block_height*width) + blockIdx.x*block_width + threadIdx.y*(width*thread_height) + threadIdx.x * thread_width;
	int pixelOffset;
	float accumulator;

	for (int y = 0; y < thread_height; y++)
	{
		// If the current row went out of image bounds - leave the cycle
		if (blockIdx.y * block_height + threadIdx.y*thread_height + y >= height)
			break;
		for (int x = 0; x < thread_width; x++)
		{
			// If the current column went out of bounds - proceed to next row
			if (blockIdx.x * block_width + threadIdx.x * thread_width + x >= width)
				break;
			
			// important
			pixelOffset = baseOffset_thread + y*width + x;
			accumulator = 0;
			for ( int indexX = -2; indexX < 3;indexX ++)
			{
				for ( int indexY = -2; indexY < 3;indexY ++)
				{
					accumulator += data[pixelOffset + indexY * width + indexX] * gaussMask[2 + indexX][2 + indexY];
				}
			}
			output[pixelOffset] = static_cast<unsigned char>(accumulator / 159);
		}
	}
}

//////// Edge detection 
__constant__ float sobelMask_horizontal[3][3] = {
	{ -1, 0, 1},
	{ -2, 0, 2},
	{ -1, 0, 1}
};
__constant__ float sobelMask_vertical[3][3] = {
	{ -1, -2, -1},
	{ 0, 0, 0},
	{ 1, 2, 1}
};
__constant__ float sharMask_horizontal[3][3] = {
	{ -3, 0, 3},
	{ -10, 0, 10},
	{ -3, 0, 3}
};
__constant__ float sharMask_vertical[3][3] = {
	{ -3, -10, -3},
	{ 0, 0, 0},
	{ 3, 10, 3}
};
__constant__ int sobelFlag;
__global__ void kernel_EdgeDetection( unsigned char *data, unsigned char *output, float *dirOutput)
{
	// if flag == true - Sobel matrix
	// if flag == false - Sharr matrix

	int baseOffset_thread = blockIdx.y*(block_height*width) + blockIdx.x*block_width + threadIdx.y*(width*thread_height) + threadIdx.x * thread_width;
	int pixelOffset;

	float accumulator_horizontal;
	float accumulator_vertical;

	float* mask_horizontal[3];
	float* mask_vertical[3];

	int normalizeMod;

	if(sobelFlag == 1)
	{
		// potomu chto c++ ne ishet legkih putey :(
		mask_horizontal[0] = &sobelMask_horizontal[0][0];
		mask_horizontal[1] = &sobelMask_horizontal[1][0];
		mask_horizontal[2] = &sobelMask_horizontal[2][0];
		mask_vertical[0] = &sobelMask_vertical[0][0];
		mask_vertical[1] = &sobelMask_vertical[1][0];
		mask_vertical[2] = &sobelMask_vertical[2][0];
		normalizeMod = 2;
	}
	else
	{
		// potomu chto c++ ne ishet legkih putey :(
		mask_horizontal[0] = &sharMask_horizontal[0][0];
		mask_horizontal[1] = &sharMask_horizontal[1][0];
		mask_horizontal[2] = &sharMask_horizontal[2][0];
		mask_vertical[0] = &sharMask_vertical[0][0];
		mask_vertical[1] = &sharMask_vertical[1][0];
		mask_vertical[2] = &sharMask_vertical[2][0];
		normalizeMod = 8;
	}

	for (int y = 0; y < thread_height; y++)
	{
		// If the current row went out of image bounds - leave the cycle
		if (blockIdx.y * block_height + threadIdx.y*thread_height + y >= height)
			break;
		for (int x = 0; x < thread_width; x++)
		{
			// If the current column went out of bounds - proceed to next row
			if (blockIdx.x * block_width + threadIdx.x * thread_width + x >= width)
				break;
			
			// important
			pixelOffset = baseOffset_thread + y*width + x;
			for ( int indexX = -1; indexX < 2; indexX ++)
			{
				for ( int indexY = -1; indexY < 2;indexY ++)
				{
					accumulator_vertical += data[pixelOffset + indexY * width + indexX] * mask_vertical[1+indexY][1+indexX];
					accumulator_horizontal += data[pixelOffset + indexY * width + indexX] * mask_horizontal[1+indexY][1+indexX];
				}
			}
			// normalizing accumulators
			accumulator_vertical /= normalizeMod;
			accumulator_horizontal /= normalizeMod;

			unsigned char tmp = static_cast<unsigned char>(sqrt(accumulator_vertical*accumulator_vertical + accumulator_horizontal*accumulator_horizontal));
			output[pixelOffset] = tmp;

			if (tmp > 15)
				dirOutput[pixelOffset] = atan2f(accumulator_vertical, accumulator_horizontal);
		}
	}
}

__global__ void kernel_radianToColor( float *data, unsigned char *output)
{
	// data - float[size]
	// output - unsigned char[size*3]

	int baseOffset_thread = blockIdx.y*(block_height*width) + blockIdx.x*block_width + threadIdx.y*(width*thread_height) + threadIdx.x * thread_width;
	int pixelOffset;
	float accumulator;

	for (int y = 0; y < thread_height; y++)
	{
		// If the current row went out of image bounds - leave the cycle
		if (blockIdx.y * block_height + threadIdx.y*thread_height + y >= height)
			break;
		for (int x = 0; x < thread_width; x++)
		{
			// If the current column went out of bounds - proceed to next row
			if (blockIdx.x * block_width + threadIdx.x * thread_width + x >= width)
				break;
			
			// important
			unsigned char tmp = static_cast<unsigned char>((data[baseOffset_thread + y*width + x] + M_PI)*57);

			if (tmp < 60 || tmp > 300)
				output[(baseOffset_thread + y*width + x)*3] = (30 - abs(30 - tmp%60))*8;
			else if (tmp < 120 || tmp > 240)
				output[(baseOffset_thread + y*width + x)*3 + 1] = (30 - abs(30 - tmp%60))*8;
			else if (tmp < 360)
				output[(baseOffset_thread + y*width + x)*3 + 2] = (30 - abs(30 - tmp%60))*8;

		}
	}


			/*
	int idX = threadIdx.x % 32; // blocks X index
	int idY = threadIdx.x / 32; // blocks Y index
	int tmpx;					// blocks first pixel X index
	int tmpy;					// blocks first pixel Y index
	
	float accumulator_horizontal = 0;
	float accumulator_vertical = 0;

	int initI, endI;			// used to avoid blurring first/last two rows and columns
	int initJ, endJ;
	initI = 0;
	endI = widthStep;			
	initJ = 0;
	endJ = heightStep;
	
	for (int i = initI; i <= endI; i++)
	{
		tmpx = i + idX * widthStep;
		if (tmpx > width)
			break;
		for (int j = initJ; j <= endJ; j++)
		{
			tmpy = j + idY * heightStep;
			if ( tmpy > height)
				break;
			

			unsigned char tmp = static_cast<unsigned char>((data[tmpy * width + tmpx] + M_PI)*81);
            if (tmp < 0)
				tmp = 0;

			//output[(tmpy * width + tmpx) + tmp/86] = tmp * M_PI;
		}
	}*/
}

