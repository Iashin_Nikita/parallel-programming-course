﻿namespace CudaKurs
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputPanel = new System.Windows.Forms.Panel();
            this.InputPanel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InterfacePanel = new System.Windows.Forms.Panel();
            this.edgeGroupbox = new System.Windows.Forms.GroupBox();
            this.sharrRadioButton = new System.Windows.Forms.RadioButton();
            this.sobelRadioButton = new System.Windows.Forms.RadioButton();
            this.edgeButton = new System.Windows.Forms.Button();
            this.gaussButton = new System.Windows.Forms.Button();
            this.grayscaleButton = new System.Windows.Forms.Button();
            this.gridGroupbox = new System.Windows.Forms.GroupBox();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.gammaGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.maxGammaTextBox = new System.Windows.Forms.TextBox();
            this.autoAdjustCheckBox = new System.Windows.Forms.CheckBox();
            this.gammaTrackBar = new System.Windows.Forms.TrackBar();
            this.AdjustButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gridXTextBox = new System.Windows.Forms.TextBox();
            this.gridYTextBox = new System.Windows.Forms.TextBox();
            this.blockXTextBox = new System.Windows.Forms.TextBox();
            this.blockYTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.InitGridButton = new System.Windows.Forms.Button();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.InterfacePanel.SuspendLayout();
            this.edgeGroupbox.SuspendLayout();
            this.gridGroupbox.SuspendLayout();
            this.gammaGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gammaTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // OutputPanel
            // 
            this.OutputPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.OutputPanel.BackColor = System.Drawing.SystemColors.Control;
            this.OutputPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OutputPanel.Location = new System.Drawing.Point(418, 27);
            this.OutputPanel.Name = "OutputPanel";
            this.OutputPanel.Size = new System.Drawing.Size(400, 400);
            this.OutputPanel.TabIndex = 1;
            this.OutputPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.OutputPanel_Paint);
            // 
            // InputPanel
            // 
            this.InputPanel.BackColor = System.Drawing.SystemColors.Control;
            this.InputPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InputPanel.Location = new System.Drawing.Point(12, 27);
            this.InputPanel.Name = "InputPanel";
            this.InputPanel.Size = new System.Drawing.Size(400, 400);
            this.InputPanel.TabIndex = 0;
            this.InputPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.InputPanel_Paint);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(828, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveImageToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.openToolStripMenuItem.Text = "Open image";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.saveImageToolStripMenuItem.Text = "Save image";
            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.saveImageToolStripMenuItem_Click);
            // 
            // InterfacePanel
            // 
            this.InterfacePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.InterfacePanel.Controls.Add(this.edgeGroupbox);
            this.InterfacePanel.Controls.Add(this.gridGroupbox);
            this.InterfacePanel.Controls.Add(this.logTextBox);
            this.InterfacePanel.Controls.Add(this.gammaGroupBox);
            this.InterfacePanel.Location = new System.Drawing.Point(0, 433);
            this.InterfacePanel.Name = "InterfacePanel";
            this.InterfacePanel.Size = new System.Drawing.Size(828, 115);
            this.InterfacePanel.TabIndex = 3;
            // 
            // edgeGroupbox
            // 
            this.edgeGroupbox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.edgeGroupbox.BackColor = System.Drawing.SystemColors.Control;
            this.edgeGroupbox.Controls.Add(this.sharrRadioButton);
            this.edgeGroupbox.Controls.Add(this.sobelRadioButton);
            this.edgeGroupbox.Controls.Add(this.edgeButton);
            this.edgeGroupbox.Controls.Add(this.gaussButton);
            this.edgeGroupbox.Controls.Add(this.grayscaleButton);
            this.edgeGroupbox.Location = new System.Drawing.Point(636, 8);
            this.edgeGroupbox.Name = "edgeGroupbox";
            this.edgeGroupbox.Size = new System.Drawing.Size(182, 100);
            this.edgeGroupbox.TabIndex = 5;
            this.edgeGroupbox.TabStop = false;
            this.edgeGroupbox.Text = "Edge Detection";
            // 
            // sharrRadioButton
            // 
            this.sharrRadioButton.AutoSize = true;
            this.sharrRadioButton.Location = new System.Drawing.Point(130, 46);
            this.sharrRadioButton.Name = "sharrRadioButton";
            this.sharrRadioButton.Size = new System.Drawing.Size(50, 17);
            this.sharrRadioButton.TabIndex = 4;
            this.sharrRadioButton.TabStop = true;
            this.sharrRadioButton.Text = "Sharr";
            this.sharrRadioButton.UseVisualStyleBackColor = true;
            // 
            // sobelRadioButton
            // 
            this.sobelRadioButton.AutoSize = true;
            this.sobelRadioButton.Checked = true;
            this.sobelRadioButton.Location = new System.Drawing.Point(82, 46);
            this.sobelRadioButton.Name = "sobelRadioButton";
            this.sobelRadioButton.Size = new System.Drawing.Size(52, 17);
            this.sobelRadioButton.TabIndex = 3;
            this.sobelRadioButton.TabStop = true;
            this.sobelRadioButton.Text = "Sobel";
            this.sobelRadioButton.UseVisualStyleBackColor = true;
            // 
            // edgeButton
            // 
            this.edgeButton.Location = new System.Drawing.Point(6, 42);
            this.edgeButton.Name = "edgeButton";
            this.edgeButton.Size = new System.Drawing.Size(75, 23);
            this.edgeButton.TabIndex = 2;
            this.edgeButton.Text = "3. Edges";
            this.edgeButton.UseVisualStyleBackColor = true;
            this.edgeButton.Click += new System.EventHandler(this.edgeButton_Click);
            // 
            // gaussButton
            // 
            this.gaussButton.Location = new System.Drawing.Point(88, 19);
            this.gaussButton.Name = "gaussButton";
            this.gaussButton.Size = new System.Drawing.Size(75, 23);
            this.gaussButton.TabIndex = 1;
            this.gaussButton.Text = "2.Gauss Blur";
            this.gaussButton.UseVisualStyleBackColor = true;
            this.gaussButton.Click += new System.EventHandler(this.gaussButton_Click);
            // 
            // grayscaleButton
            // 
            this.grayscaleButton.Location = new System.Drawing.Point(6, 19);
            this.grayscaleButton.Name = "grayscaleButton";
            this.grayscaleButton.Size = new System.Drawing.Size(75, 23);
            this.grayscaleButton.TabIndex = 0;
            this.grayscaleButton.Text = "1.Grayscale";
            this.grayscaleButton.UseVisualStyleBackColor = true;
            this.grayscaleButton.Click += new System.EventHandler(this.grayscaleButton_Click);
            // 
            // gridGroupbox
            // 
            this.gridGroupbox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.gridGroupbox.BackColor = System.Drawing.SystemColors.Control;
            this.gridGroupbox.Controls.Add(this.InitGridButton);
            this.gridGroupbox.Controls.Add(this.label7);
            this.gridGroupbox.Controls.Add(this.label6);
            this.gridGroupbox.Controls.Add(this.label5);
            this.gridGroupbox.Controls.Add(this.label4);
            this.gridGroupbox.Controls.Add(this.blockYTextBox);
            this.gridGroupbox.Controls.Add(this.blockXTextBox);
            this.gridGroupbox.Controls.Add(this.gridYTextBox);
            this.gridGroupbox.Controls.Add(this.gridXTextBox);
            this.gridGroupbox.Controls.Add(this.label3);
            this.gridGroupbox.Controls.Add(this.label2);
            this.gridGroupbox.Location = new System.Drawing.Point(249, 6);
            this.gridGroupbox.Name = "gridGroupbox";
            this.gridGroupbox.Size = new System.Drawing.Size(174, 100);
            this.gridGroupbox.TabIndex = 4;
            this.gridGroupbox.TabStop = false;
            this.gridGroupbox.Text = "Grid settings";
            // 
            // logTextBox
            // 
            this.logTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.logTextBox.Location = new System.Drawing.Point(11, 6);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTextBox.Size = new System.Drawing.Size(232, 104);
            this.logTextBox.TabIndex = 3;
            // 
            // gammaGroupBox
            // 
            this.gammaGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.gammaGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.gammaGroupBox.Controls.Add(this.label1);
            this.gammaGroupBox.Controls.Add(this.maxGammaTextBox);
            this.gammaGroupBox.Controls.Add(this.autoAdjustCheckBox);
            this.gammaGroupBox.Controls.Add(this.gammaTrackBar);
            this.gammaGroupBox.Controls.Add(this.AdjustButton);
            this.gammaGroupBox.Location = new System.Drawing.Point(429, 8);
            this.gammaGroupBox.Name = "gammaGroupBox";
            this.gammaGroupBox.Size = new System.Drawing.Size(200, 98);
            this.gammaGroupBox.TabIndex = 2;
            this.gammaGroupBox.TabStop = false;
            this.gammaGroupBox.Text = "Gamma Correction";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Max gamma offset:";
            // 
            // maxGammaTextBox
            // 
            this.maxGammaTextBox.Location = new System.Drawing.Point(114, 66);
            this.maxGammaTextBox.Name = "maxGammaTextBox";
            this.maxGammaTextBox.Size = new System.Drawing.Size(80, 20);
            this.maxGammaTextBox.TabIndex = 3;
            this.maxGammaTextBox.TextChanged += new System.EventHandler(this.maxGammaTextBox_TextChanged);
            // 
            // autoAdjustCheckBox
            // 
            this.autoAdjustCheckBox.AutoSize = true;
            this.autoAdjustCheckBox.Location = new System.Drawing.Point(120, 42);
            this.autoAdjustCheckBox.Name = "autoAdjustCheckBox";
            this.autoAdjustCheckBox.Size = new System.Drawing.Size(75, 17);
            this.autoAdjustCheckBox.TabIndex = 2;
            this.autoAdjustCheckBox.Text = "autoadjust";
            this.autoAdjustCheckBox.UseVisualStyleBackColor = true;
            this.autoAdjustCheckBox.CheckedChanged += new System.EventHandler(this.autoAdjustCheckBox_CheckedChanged);
            // 
            // gammaTrackBar
            // 
            this.gammaTrackBar.Location = new System.Drawing.Point(7, 19);
            this.gammaTrackBar.Maximum = 50;
            this.gammaTrackBar.Minimum = -50;
            this.gammaTrackBar.Name = "gammaTrackBar";
            this.gammaTrackBar.Size = new System.Drawing.Size(104, 45);
            this.gammaTrackBar.TabIndex = 1;
            this.gammaTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.gammaTrackBar.ValueChanged += new System.EventHandler(this.gammaTrackBar_ValueChanged);
            // 
            // AdjustButton
            // 
            this.AdjustButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.AdjustButton.Location = new System.Drawing.Point(119, 19);
            this.AdjustButton.Name = "AdjustButton";
            this.AdjustButton.Size = new System.Drawing.Size(75, 23);
            this.AdjustButton.TabIndex = 0;
            this.AdjustButton.Text = "Adjust";
            this.AdjustButton.UseVisualStyleBackColor = true;
            this.AdjustButton.Click += new System.EventHandler(this.AdjustButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Grid size";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Block size";
            // 
            // gridXTextBox
            // 
            this.gridXTextBox.Location = new System.Drawing.Point(29, 35);
            this.gridXTextBox.Name = "gridXTextBox";
            this.gridXTextBox.Size = new System.Drawing.Size(43, 20);
            this.gridXTextBox.TabIndex = 2;
            // 
            // gridYTextBox
            // 
            this.gridYTextBox.Location = new System.Drawing.Point(29, 56);
            this.gridYTextBox.Name = "gridYTextBox";
            this.gridYTextBox.Size = new System.Drawing.Size(43, 20);
            this.gridYTextBox.TabIndex = 3;
            // 
            // blockXTextBox
            // 
            this.blockXTextBox.Location = new System.Drawing.Point(106, 35);
            this.blockXTextBox.Name = "blockXTextBox";
            this.blockXTextBox.Size = new System.Drawing.Size(43, 20);
            this.blockXTextBox.TabIndex = 4;
            // 
            // blockYTextBox
            // 
            this.blockYTextBox.Location = new System.Drawing.Point(106, 56);
            this.blockYTextBox.Name = "blockYTextBox";
            this.blockYTextBox.Size = new System.Drawing.Size(43, 20);
            this.blockYTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "X:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(78, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "X:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(78, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Y:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Y:";
            // 
            // InitGridButton
            // 
            this.InitGridButton.Location = new System.Drawing.Point(18, 77);
            this.InitGridButton.Name = "InitGridButton";
            this.InitGridButton.Size = new System.Drawing.Size(140, 20);
            this.InitGridButton.TabIndex = 10;
            this.InitGridButton.Text = "Initialize";
            this.InitGridButton.UseVisualStyleBackColor = true;
            this.InitGridButton.Click += new System.EventHandler(this.InitGridButton_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetImageToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // resetImageToolStripMenuItem
            // 
            this.resetImageToolStripMenuItem.Name = "resetImageToolStripMenuItem";
            this.resetImageToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.resetImageToolStripMenuItem.Text = "Reset image";
            this.resetImageToolStripMenuItem.Click += new System.EventHandler(this.resetImageToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(828, 549);
            this.Controls.Add(this.InterfacePanel);
            this.Controls.Add(this.InputPanel);
            this.Controls.Add(this.OutputPanel);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "CUDA Filters test";
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.InterfacePanel.ResumeLayout(false);
            this.InterfacePanel.PerformLayout();
            this.edgeGroupbox.ResumeLayout(false);
            this.edgeGroupbox.PerformLayout();
            this.gridGroupbox.ResumeLayout(false);
            this.gridGroupbox.PerformLayout();
            this.gammaGroupBox.ResumeLayout(false);
            this.gammaGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gammaTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel OutputPanel;
        private System.Windows.Forms.Panel InputPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.Panel InterfacePanel;
        private System.Windows.Forms.Button AdjustButton;
        private System.Windows.Forms.GroupBox gammaGroupBox;
        private System.Windows.Forms.TrackBar gammaTrackBar;
        private System.Windows.Forms.CheckBox autoAdjustCheckBox;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox maxGammaTextBox;
        private System.Windows.Forms.GroupBox edgeGroupbox;
        private System.Windows.Forms.GroupBox gridGroupbox;
        private System.Windows.Forms.Button grayscaleButton;
        private System.Windows.Forms.Button gaussButton;
        private System.Windows.Forms.Button edgeButton;
        private System.Windows.Forms.RadioButton sharrRadioButton;
        private System.Windows.Forms.RadioButton sobelRadioButton;
        private System.Windows.Forms.Button InitGridButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox blockYTextBox;
        private System.Windows.Forms.TextBox blockXTextBox;
        private System.Windows.Forms.TextBox gridYTextBox;
        private System.Windows.Forms.TextBox gridXTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetImageToolStripMenuItem;


    }
}

