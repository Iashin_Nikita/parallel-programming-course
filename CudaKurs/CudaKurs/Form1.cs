﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using System.Diagnostics;

using ManagedCuda;
using ManagedCuda.BasicTypes;
using ManagedCuda.VectorTypes;

namespace CudaKurs
{
    public partial class Form1 : Form
    {
        #region Variables
        // UI vars
        int bottomBarOffset = 200;
        float maxGammaOffset = 0.15f;
        List<Button> grayscaleDepButtons;   // initialized in InitUI()

        // Image vars
        Bitmap image;
        Rectangle imageRect;
        ColorPalette grayscalePalette;
        ColorPalette mapped8bitPalette;
        int imageStride;
        byte[] rgbValues_original;
        byte[] rgbValues_edited;
        float[] sobelGradientDirections;
        byte[] sobelGradientDirectionsRGB;
        Bitmap outputImage;
        bool outputImage_8bpp = false;
        OutputImageState _imgState;
        OutputImageState imgState
        {
            get
            {
                return _imgState;
            }
            set
            {
                _imgState = value;
                outputImage_8bpp = _imgState == OutputImageState.ORIGINAL24BPP ? false : true;
            }
        }

        // CUDA vars
        CudaContext cudaContext;
        CUmodule cudaModule;
        CudaKernel kernel_gamma;
        CudaKernel[] kernels_edgeDetection;
        CudaDeviceProperties deviceInfo;
        dim3 grid;
        dim3 block;
        #endregion

        #region General functions
        public Form1()
        {
            InitializeComponent();
            InitializeCuda();
            InitializeUI();
        }

        private void InitializeCuda()
        {
            // Creating Cuda context for the specific gpu
            cudaContext = new CudaContext(0);

            // Log device info
            deviceInfo = cudaContext.GetDeviceInfo();
            Log("Device:  " + deviceInfo.DeviceName);
            Log("MaxBlockDim:  " + deviceInfo.MaxBlockDim.ToString());
            Log("MaxGridDim:  " + deviceInfo.MaxGridDim.ToString());
            Log("MaxRegistersPerMultiproc:   " + deviceInfo.MaxRegistersPerMultiprocessor.ToString());
            Log("MaxThreadsPerBlock:  " + deviceInfo.MaxThreadsPerBlock.ToString());
            Log("MaxThreadsPerMultiproc:  " + deviceInfo.MaxThreadsPerMultiProcessor.ToString());
            Log("MultiprocCount:  " + deviceInfo.MultiProcessorCount.ToString());
            Log("WarpSize:  " + deviceInfo.WarpSize.ToString());

            //Loading Kernels from file
            cudaModule = cudaContext.LoadModule("kernel.ptx");

            kernel_gamma = new CudaKernel("_Z12kernel_gammaPh", cudaModule, cudaContext);
            kernels_edgeDetection = new CudaKernel[4];
            kernels_edgeDetection[0] = new CudaKernel("_Z16kernel_grayscalePhS_", cudaModule, cudaContext);
            kernels_edgeDetection[1] = new CudaKernel("_Z20kernel_gaussOverGrayPhS_", cudaModule, cudaContext);
            kernels_edgeDetection[2] = new CudaKernel("_Z20kernel_EdgeDetectionPhS_Pf", cudaModule, cudaContext);
            kernels_edgeDetection[3] = new CudaKernel("_Z20kernel_radianToColorPfPh", cudaModule, cudaContext);

            // Creating default grid
            SetKernelGrid(deviceInfo.MultiProcessorCount, 1, 32, 32);
        }
        private void SetKernelGrid(int gridX, int gridY, int blockX, int blockY)
        {
            // Checking if params are valid
            if (gridX < 1 || gridY < 1 || blockX < 1 || blockY < 1 || blockX/blockY > 1024)
            {
                Log("Invalid grid/block sizes");
                return;
            }

            grid = new dim3(gridX, gridY);
            block = new dim3(blockX, blockY);

            // Setting grids for all kernels
            kernel_gamma.GridDimensions = new dim3(gridX, gridY);
            kernel_gamma.BlockDimensions = new dim3(blockX, blockY);
            for (int i = 0; i < kernels_edgeDetection.Length; i++)
            {
                kernels_edgeDetection[i].GridDimensions = grid;
                kernels_edgeDetection[i].BlockDimensions = block;
            }

            Log("Grid was set to (" + grid.x + "," + grid.y + ")");
            Log("Blocks were set to (" + block.x + "," + block.y + ")");
        }
        private void SetKernelConstants(bool bpp8)
        {
            // Finding kernel constants
            int width = bpp8? imageRect.Width : imageStride;
            int height = imageRect.Height;
            int threadHeight = (int)Math.Ceiling((double)height / (block.y * grid.y));
            int threadWidth = (int)Math.Ceiling((double)width / (block.x * grid.x));
            int blockHeight = (int)(threadHeight * block.y);
            int blockWidth = (int)(threadWidth * block.x);

            // Setting kernel constants
            kernel_gamma.SetConstantVariable<int>("width", width);
            kernel_gamma.SetConstantVariable<int>("height", height);
            kernel_gamma.SetConstantVariable<int>("block_height", blockHeight);
            kernel_gamma.SetConstantVariable<int>("block_width", blockWidth);
            kernel_gamma.SetConstantVariable<int>("thread_height", threadHeight);
            kernel_gamma.SetConstantVariable<int>("thread_width", threadWidth);
        }
        private void InitializeUI()
        {
            grayscaleDepButtons = new List<Button>();
            grayscaleDepButtons.Add(gaussButton);
            grayscaleDepButtons.Add(edgeButton);

            maxGammaTextBox.Text = maxGammaOffset.ToString();

            // Fixing flickering ( from stackoverflow ;) )
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                        null, InputPanel, new object[] { true });
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                                        null, OutputPanel, new object[] { true });

            // weird workaround to create a grayscale palette, since it can't be created directly
            var myBMP = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            grayscalePalette = myBMP.Palette;
            Color[] _entries = grayscalePalette.Entries;
            for (int i = 0; i < 256; i++)
            {
                _entries[i] = Color.FromArgb((byte)i, (byte)i, (byte)i);
            }
            // same for mapped palette
            myBMP = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            mapped8bitPalette = myBMP.Palette;
            _entries = mapped8bitPalette.Entries;
            for (int i = 0; i < 256; i++)
            {
                switch (i / 85)
                {
                    case 0:
                        _entries[i] = Color.FromArgb((byte)(i * 3), 0, 0);
                        break;
                    case 1:
                        _entries[i] = Color.FromArgb(0, (byte)(i * 3), 0);
                        break;
                    case 2:
                        _entries[i] = Color.FromArgb(0, 0, (byte)(i * 3));
                        break;
                }
            }


            RefreshUI();
        }

        private void PrepareInputImage()
        {
            // Lock the bitmap's bits.  
            imageRect = new Rectangle(0, 0, image.Width, image.Height);
            BitmapData imageData = image.LockBits(imageRect, System.Drawing.Imaging.ImageLockMode.ReadOnly, image.PixelFormat);
            // Declare an array to hold the bytes of the bitmap.
            imageStride = Math.Abs(imageData.Stride);
            int bytes = imageStride * image.Height;
            rgbValues_original = new byte[bytes];
            rgbValues_edited = new byte[bytes];
            // Copy the RGB values into the array.
            Marshal.Copy(imageData.Scan0, rgbValues_original, 0, bytes);
            Marshal.Copy(imageData.Scan0, rgbValues_edited, 0, bytes);
            // Unlock the bits.
            image.UnlockBits(imageData);
            imgState = OutputImageState.ORIGINAL24BPP;
        }
        private void PrepareOutputImage()
        {
            if (imgState == OutputImageState.GRAY8BPP || 
                imgState == OutputImageState.BLURED8BPP || 
                imgState == OutputImageState.EDGE8BPP)
            {
                if (outputImage != null)
                    outputImage.Dispose();
                outputImage = new Bitmap(image.Width, image.Height, PixelFormat.Format8bppIndexed);
                BitmapData outputImageData = outputImage.LockBits(imageRect, ImageLockMode.WriteOnly, outputImage.PixelFormat);
                Marshal.Copy(rgbValues_edited, 0, outputImageData.Scan0, rgbValues_edited.Length);
                outputImage.UnlockBits(outputImageData);
                outputImage.Palette = grayscalePalette;
            }
            else if (imgState == OutputImageState.ORIGINAL24BPP)
            {
                if (outputImage != null)
                    outputImage.Dispose();
                outputImage = new Bitmap(image.Width, image.Height, image.PixelFormat);
                BitmapData outputImageData = outputImage.LockBits(imageRect, ImageLockMode.WriteOnly, outputImage.PixelFormat);
                Marshal.Copy(rgbValues_edited, 0, outputImageData.Scan0, rgbValues_edited.Length);
                outputImage.UnlockBits(outputImageData);
            }
            else if (imgState == OutputImageState.EDGEDIR24BPP)
            {
                if (outputImage != null)
                    outputImage.Dispose();
                outputImage = new Bitmap(image.Width, image.Height, image.PixelFormat);
                BitmapData outputImageData = outputImage.LockBits(imageRect, ImageLockMode.WriteOnly, image.PixelFormat);
                Marshal.Copy(sobelGradientDirectionsRGB, 0, outputImageData.Scan0, sobelGradientDirectionsRGB.Length);
                outputImage.UnlockBits(outputImageData);
            }
        }

        private void Log(string message)
        {
            logTextBox.Text = "[" + DateTime.Now.TimeOfDay.Hours + ":" + DateTime.Now.TimeOfDay.Minutes +
                ":" + DateTime.Now.TimeOfDay.Seconds + "]:  " + message + "\r\n" + logTextBox.Text;

            this.Refresh();
        }

        private void RefreshUI()
        {
            foreach (Button button in grayscaleDepButtons)
            {
                button.Enabled = outputImage_8bpp;
            }

            InputPanel.Invalidate();
            OutputPanel.Invalidate();
        }
        #endregion

        #region GPU functions
        // TODO: MOVE CONSTANT TRANSFERS TO InitCUDA
        private byte[] AdjustGamma(byte[] rgbArray, float gammaMult, bool logTime)
        {
            // Mesuring overall time of the operation
            Stopwatch swTotal = new Stopwatch();
            // Measuring how long did GPU calculations take
            Stopwatch swCalc = new Stopwatch();

            if (logTime) swTotal.Start();
            // CudaDeviceVariable automatically allocates memory on the device
            int size = rgbArray.Length;
            CudaDeviceVariable<byte> data_device = new CudaDeviceVariable<byte>(size);

            // Preparing  constant memory
            SetKernelConstants(outputImage_8bpp);
            kernel_gamma.SetConstantVariable<float>("gammaCorRatio", gammaMult);
            
            // Copying data from host to device
            data_device.CopyToDevice(rgbArray);

            // Launching Kernel
            if (logTime)
            {
                swCalc.Start();
                kernel_gamma.Run(data_device.DevicePointer);
                swCalc.Stop();
            }
            else
                kernel_gamma.Run(data_device.DevicePointer);

            // Returning result from device
            byte[] result = new byte[rgbArray.Length];
            data_device.CopyToHost(result);

            if (logTime)
            {
                swTotal.Stop();
                Log("Gamma adjustment took " + swTotal.ElapsedMilliseconds.ToString() + " ms (" +
                    swCalc.ElapsedMilliseconds.ToString() + ")");
            }
            return result;
        }
        private byte[] ToGrayscale(byte[] rgbArray, bool logTime)
        {
            // Mesuring overall time of the operation
            Stopwatch swTotal = new Stopwatch();
            // Measuring how long did GPU calculations take
            Stopwatch swCalc = new Stopwatch();

            if (logTime) swTotal.Start();
            // CudaDeviceVariable automatically allocates memory on the device
            int size = rgbArray.Length;
            CudaDeviceVariable<byte> imageData_device = new CudaDeviceVariable<byte>(size);
            CudaDeviceVariable<byte> grayscale_device = new CudaDeviceVariable<byte>(size/3);
            // Preparing variables in constant memory
            SetKernelConstants(false);

            // Copying data from host to device
            imageData_device.CopyToDevice(rgbArray);

            // Launching Kernel
            if (logTime)
            {
                swCalc.Start();
                kernels_edgeDetection[0].Run(imageData_device.DevicePointer, grayscale_device.DevicePointer);
                swCalc.Stop();
            }
            else
                kernels_edgeDetection[0].Run(imageData_device.DevicePointer, grayscale_device.DevicePointer);

            // Returning result from device
            byte[] result = new byte[rgbArray.Length / 3];
            grayscale_device.CopyToHost(result);

            if (logTime)
            {
                swTotal.Stop();
                Log("Grayscale took " + swTotal.ElapsedMilliseconds.ToString() + " ms (" +
                    swCalc.ElapsedMilliseconds.ToString() + ")");
            }
            return result;
        }
        private byte[] GaussBlur(byte[] grayscaleArray, bool logTime)
        {
            // Mesuring overall time of the operation
            Stopwatch swTotal = new Stopwatch();
            // Measuring how long did GPU calculations take
            Stopwatch swCalc = new Stopwatch();

            if (logTime) swTotal.Start();
            // CudaDeviceVariable automatically allocates memory on the device
            int size = grayscaleArray.Length;
            CudaDeviceVariable<byte> imageData_device = new CudaDeviceVariable<byte>(size);
            CudaDeviceVariable<byte> gauss_device = new CudaDeviceVariable<byte>(size);
            // Preparing variables in constant memory
            SetKernelConstants(outputImage_8bpp);
            // Copying data from host to device
            imageData_device.CopyToDevice(grayscaleArray);

            // Launching Kernel
            if (logTime)
            {
                swCalc.Start();
                kernels_edgeDetection[1].Run(imageData_device.DevicePointer, gauss_device.DevicePointer);
                swCalc.Stop();
            }
            else
                kernels_edgeDetection[1].Run(imageData_device.DevicePointer, gauss_device.DevicePointer);

            // Returning result from device
            byte[] result = new byte[grayscaleArray.Length];
            gauss_device.CopyToHost(result);

            if (logTime)
            {
                swTotal.Stop();
                Log("Gauss blur took " + swTotal.ElapsedMilliseconds.ToString() + " ms (" +
                    swCalc.ElapsedMilliseconds.ToString() + ")");
            }

            return result;
        }
        private byte[] EdgeDetectionAlgorithm(byte[] grayscaleArray, bool logTime)
        {
            // Mesuring overall time of the operation
            Stopwatch swTotal = new Stopwatch();
            // Measuring how long did GPU calculations take
            Stopwatch swCalc = new Stopwatch();

            if (logTime) swTotal.Start();
            // CudaDeviceVariable automatically allocates memory on the device
            int size = grayscaleArray.Length;
            CudaDeviceVariable<byte> imageData_device = new CudaDeviceVariable<byte>(size);
            CudaDeviceVariable<byte> gauss_device = new CudaDeviceVariable<byte>(size);
            CudaDeviceVariable<float> sobelDirections_device = new CudaDeviceVariable<float>(size);
            // Preparing variables in constant memory
            SetKernelConstants(outputImage_8bpp);
            kernels_edgeDetection[2].SetConstantVariable<int>("sobelFlag", sobelRadioButton.Checked?1:0);
            // Copying data from host to device
            imageData_device.CopyToDevice(grayscaleArray);

            // Launching Kernel
            if (logTime)
            {
                swCalc.Start();
                kernels_edgeDetection[2].Run(imageData_device.DevicePointer, gauss_device.DevicePointer, sobelDirections_device.DevicePointer);
                swCalc.Stop();
            }
            else
                kernels_edgeDetection[2].Run(imageData_device.DevicePointer, gauss_device.DevicePointer, sobelDirections_device.DevicePointer, sobelRadioButton.Checked);

            // Returning result from device
            byte[] result = new byte[grayscaleArray.Length];
            gauss_device.CopyToHost(result);

            sobelGradientDirections = new float[size];
            sobelDirections_device.CopyToHost(sobelGradientDirections);

            if (logTime)
            {
                swTotal.Stop();
                Log("Sobel algorithm took " + swTotal.ElapsedMilliseconds.ToString() + " ms (" +
                    swCalc.ElapsedMilliseconds.ToString() + ")");
            }

            return result;
        }
        private byte[] RadianToRGB(float[] radianArray, bool logTime)
        {
            // Mesuring overall time of the operation
            Stopwatch swTotal = new Stopwatch();
            // Measuring how long did GPU calculations take
            Stopwatch swCalc = new Stopwatch();

            if (logTime) swTotal.Start();
            // CudaDeviceVariable automatically allocates memory on the device
            int size = radianArray.Length;
            CudaDeviceVariable<float> imageData_device = new CudaDeviceVariable<float>(size);
            CudaDeviceVariable<byte> rgb_device = new CudaDeviceVariable<byte>(size*3);
            // Preparing variables in constant memory
            SetKernelConstants(outputImage_8bpp);
            // Copying data from host to device
            imageData_device.CopyToDevice(radianArray);

            // Launching Kernel
            if (logTime)
            {
                swCalc.Start();
                kernels_edgeDetection[3].Run(imageData_device.DevicePointer, rgb_device.DevicePointer);
                swCalc.Stop();
            }
            else
                kernels_edgeDetection[3].Run(imageData_device.DevicePointer, rgb_device.DevicePointer);

            // Returning result from device
            byte[] result = new byte[size*3];
            rgb_device.CopyToHost(result);

            if (logTime)
            {
                swTotal.Stop();
                Log("Radian to RGB took " + swTotal.ElapsedMilliseconds.ToString() + " ms (" +
                    swCalc.ElapsedMilliseconds.ToString() + ")");
            }
            return result;
        }

        #endregion

        #region Event Handlers
        // Drawing images
        int drawOffset = 2;
        private void InputPanel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
            if (image != null)
            {
                if (image.Width > image.Height)
                    e.Graphics.DrawImage(image, 0, 0, InputPanel.Size.Width - drawOffset, (image.Height * InputPanel.Size.Width / image.Width) - drawOffset);
                else
                    e.Graphics.DrawImage(image, 0, 0, (image.Width * InputPanel.Size.Height / image.Height) - drawOffset, InputPanel.Size.Height - drawOffset);
            }
        }
        private void OutputPanel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
            if (outputImage != null)
            {
                if (outputImage.Width > outputImage.Height)
                    e.Graphics.DrawImage(outputImage, 0, 0, InputPanel.Size.Width - drawOffset, (outputImage.Height * InputPanel.Size.Width / outputImage.Width) - drawOffset);
                else
                    e.Graphics.DrawImage(outputImage, 0, 0, (outputImage.Width * InputPanel.Size.Height / outputImage.Height) - drawOffset, InputPanel.Size.Height - drawOffset);
            }
        }

        // Adjusting image panels after form resize
        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            //556
            if (this.Size.Width < 844)
            {
                this.Size = new Size(844, this.Size.Height);
                logTextBox.Size = new Size(232, logTextBox.Size.Height);
            }
            else
            {
                logTextBox.Size = new Size(InterfacePanel.Size.Width - 595, logTextBox.Size.Height);
            }

            Size size;
            if (image == null)
                size = new Size(this.Size.Width / 2 - 24, this.Size.Height - bottomBarOffset);
            else
                size = new Size(this.Size.Width / 2 - 24, (this.Size.Width / 2 - 24) * imageRect.Height / imageRect.Width);

            InputPanel.Size = size;
            OutputPanel.Size = size;

            this.Size = new Size(this.Size.Width, size.Height + bottomBarOffset);

            RefreshUI();
        }
        FormWindowState LastWindowState = FormWindowState.Minimized;
        private void Form1_Resize(object sender, EventArgs e)
        {
            // When window state changes
            if (WindowState != LastWindowState)
            {
                LastWindowState = WindowState;
                Form1_ResizeEnd(this, new EventArgs());
            }

        }

        // Top bar
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    image = new Bitmap(openFileDialog1.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read image. Original error: " + ex.Message);
                    return;
                }
                PrepareInputImage();
                PrepareOutputImage();
                Form1_ResizeEnd(this, new EventArgs());
                RefreshUI();
            }
        }
        private void saveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "bmp files (*.bmp)|*.bmp|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                outputImage.Save(saveFileDialog1.FileName);
            }
        }
        private void resetImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rgbValues_edited = rgbValues_original;
            imgState = OutputImageState.ORIGINAL24BPP;
            PrepareOutputImage();
            RefreshUI();
        }

        // Buttons
        private void InitGridButton_Click(object sender, EventArgs e)
        {
            int gx, gy, bx, by;
            try
            {
                gx = Convert.ToInt32(gridXTextBox.Text);
                gy = Convert.ToInt32(gridYTextBox.Text);
                bx = Convert.ToInt32(blockXTextBox.Text);
                by = Convert.ToInt32(blockYTextBox.Text);
            }
            catch (Exception exc)
            {
                Log("Grid variables were incalid: " + exc.Message);
                return;
            }
            SetKernelGrid(gx, gy, bx, by);
        }

        private void AdjustButton_Click(object sender, EventArgs e)
        {
            // If an image wasn't open yet, trying to open it
            if (image == null)
                openToolStripMenuItem_Click(this, new EventArgs());

            // Adjusting gamma with cuda
            float newGammaVal = 1 + (float)gammaTrackBar.Value / gammaTrackBar.Maximum * maxGammaOffset;

            rgbValues_edited = AdjustGamma(rgbValues_edited, newGammaVal, true);

            PrepareOutputImage();

            Form1_ResizeEnd(this, new EventArgs());
            RefreshUI();
        }
        private void gammaTrackBar_ValueChanged(object sender, EventArgs e)
        {
            if (autoAdjustCheckBox.Checked)
            {
                float newGammaVal = 1 + (float)gammaTrackBar.Value / gammaTrackBar.Maximum * maxGammaOffset;
                rgbValues_edited = AdjustGamma(rgbValues_original, newGammaVal, false);

                imgState = OutputImageState.ORIGINAL24BPP;

                PrepareOutputImage();

                RefreshUI();
            }
        }
        private void maxGammaTextBox_TextChanged(object sender, EventArgs e)
        {
            float tmp;
            try
            {
                tmp = Convert.ToSingle(maxGammaTextBox.Text);
            }
            catch (Exception ex)
            {
                maxGammaTextBox.Text = maxGammaOffset.ToString();
                return;
            }
            maxGammaOffset = tmp;
        }
        private void autoAdjustCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (image == null)
                autoAdjustCheckBox.Checked = false;
        }

        private void grayscaleButton_Click(object sender, EventArgs e)
        {
            // If an image wasn't open yet, trying to open it
            if (image == null)
                openToolStripMenuItem_Click(this, new EventArgs());

            if (imgState == OutputImageState.ORIGINAL24BPP)
                rgbValues_edited = ToGrayscale(rgbValues_edited, true);
            else
                rgbValues_edited = ToGrayscale(rgbValues_original, true);

            imgState = OutputImageState.GRAY8BPP;

            PrepareOutputImage();

            Form1_ResizeEnd(this, new EventArgs());
            RefreshUI();
        }
        private void gaussButton_Click(object sender, EventArgs e)
        {
            if (imgState != OutputImageState.ORIGINAL24BPP)
            {
                rgbValues_edited = GaussBlur(rgbValues_edited, true);

                imgState = OutputImageState.BLURED8BPP;

                PrepareOutputImage();

                Form1_ResizeEnd(this, new EventArgs());
                RefreshUI();
            }
            else
                Log("Error: wrong image type");
        }
        private void edgeButton_Click(object sender, EventArgs e)
        {
            // If Sobel alg was already used - switching between result representation types
            if (imgState == OutputImageState.EDGE8BPP)
            {
                imgState = OutputImageState.EDGEDIR24BPP;
            }
            else if (imgState == OutputImageState.EDGEDIR24BPP)
            {
                imgState = OutputImageState.EDGE8BPP;
            }
            // If it wasn't used yet - running the algorithm
            else
            {
                rgbValues_edited = EdgeDetectionAlgorithm(rgbValues_edited, true);
                sobelGradientDirectionsRGB = RadianToRGB(sobelGradientDirections, true);
                imgState = OutputImageState.EDGE8BPP;
            }

            PrepareOutputImage();

            Form1_ResizeEnd(this, new EventArgs());
            RefreshUI();
        }
        #endregion
    }
    public enum OutputImageState
    {
        ORIGINAL24BPP,
        GRAY8BPP,
        BLURED8BPP,
        EDGE8BPP,
        EDGEDIR24BPP
    }
}
