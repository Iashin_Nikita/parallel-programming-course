﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            List<int> primes;
            int range = 100000;
            int amountOfThreads = 8;

            while (true)
            {
                Console.WriteLine("********************************************");
                Console.WriteLine("Current range for finding primes: " + range);
                Console.WriteLine("Amount of threads for parallel algorithms: " + amountOfThreads);
                Console.WriteLine("1. Test ST Masked Sieve");
                Console.WriteLine("2. Test ST Original Sieve");
                Console.WriteLine("3. Test ST Original Sieve - Modified");
                Console.WriteLine("4. Test MT1 Original Sieve - Modified");
                Console.WriteLine("5. Test MT1 Masked Sieve");
                Console.WriteLine("6. Test MT2 Original Sieve - Modified");
                Console.WriteLine("01. Set prime range");
                Console.WriteLine("02. Set thread count");
                Console.Write(">>> ");
                switch (Console.ReadLine())
                {
                    case "1":
                        watch.Reset();
                        watch.Start();
                        primes = SieveMasked(range);
                        watch.Stop();
                        Console.WriteLine("\nPrime count: " + primes.Count);
                        Console.WriteLine("Time elapsed: " + watch.Elapsed.TotalMilliseconds + "ms\n");
                        break;
                    case "2":
                        watch.Reset();
                        watch.Start();
                        primes = SieveOriginal(range);
                        watch.Stop();
                        Console.WriteLine("\nPrime count: " + primes.Count);
                        Console.WriteLine("Time elapsed: " + watch.Elapsed.TotalMilliseconds + "ms\n");
                        break;
                    case "3":
                        watch.Reset();
                        watch.Start();
                        primes = SieveOriginal_Modified(range);
                        watch.Stop();
                        Console.WriteLine("\nPrime count: " + primes.Count);
                        Console.WriteLine("Time elapsed: " + watch.Elapsed.TotalMilliseconds + "ms\n");
                        break;
                    case "4":
                        watch.Reset();
                        watch.Start();
                        primes = SieveOriginal_MT1(range, amountOfThreads);
                        watch.Stop();
                        Console.WriteLine("\nPrime count: " + primes.Count);
                        Console.WriteLine("Time elapsed: " + watch.Elapsed.TotalMilliseconds + "ms\n");
                        break;
                    case "5":
                        watch.Reset();
                        watch.Start();
                        primes = SieveMasked_MT1(range, amountOfThreads);
                        watch.Stop();
                        Console.WriteLine("\nPrime count: " + primes.Count);
                        Console.WriteLine("Time elapsed: " + watch.Elapsed.TotalMilliseconds + "ms\n");
                        break;
                    case "6":
                        watch.Reset();
                        watch.Start();
                        primes = SieveOriginal_MT2(range, amountOfThreads);
                        watch.Stop();
                        Console.WriteLine("\nPrime count: " + primes.Count);
                        Console.WriteLine("Time elapsed: " + watch.Elapsed.TotalMilliseconds + "ms\n");
                        break;

                    case "01":
                        Console.Write("New prime range: ");
                        try
                        {
                            range = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Invalid input\n");
                        }
                        break;
                    case "02":
                        Console.Write("New thread count: ");
                        try
                        {
                            amountOfThreads = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Invalid input\n");
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public static List<int> SieveMasked(int max)
        {
            // initialize variables
            List<int> result = new List<int>();
            bool[] primes = new bool[max];
            for (int i = 0; i < max; i++)
                primes[i] = true;

            // initial prime numbers
            result.Add(2);

            // main cycle
            for (int i = 3; i < max; i = i + 2)
            {
                if (primes[i] == true)
                {
                    result.Add(i);
                    for (int j = i; j < max; j = j + i)
                    {
                        primes[j] = false;
                    }
                }
            }

            return result;
        }
        public static List<int> SieveOriginal(int max)
        {
            // initialize variables
            List<int> result = new List<int>();
            bool flag = false;

            // initial prime numbers
            result.Add(2);

            // main cycle
            for (int i = 3; i < max; i = i + 2)
            {
                flag = false; 
                for (int j = 0; j < result.Count; j++)
                {
                    if (i % result[j] == 0)
                    {
                        flag = true;
                        break;
                    }
                }
                // if current number wasn't divided by any already saved prime - it is a new prime
                if (!flag)
                    result.Add(i);
            }

            return result;
        }
        public static List<int> SieveOriginal_Modified(int max)
        {
            // initialize variables
            List<int> result = new List<int>();
            List<int> basePrimes = new List<int>();
            int threshold = (int)Math.Ceiling( Math.Sqrt(max));
            threshold = threshold % 2 == 0 ? threshold + 1 : threshold;  // threshold has to be odd
            bool flag = false;

            // initial prime numbers
            basePrimes.Add(2);

            // first cycle
            for (int i = 3; i < threshold; i = i + 2)
            {
                flag = false;
                for (int j = 0; j < basePrimes.Count; j++)
                {
                    if (i % basePrimes[j] == 0)
                    {
                        flag = true;
                        break;
                    }
                }
                // if current number wasn't divided by any already saved prime - it is a new prime
                if (!flag)
                {
                    basePrimes.Add(i);
                }
            }

            // copying base primes to result
            result.AddRange(basePrimes);

            // second cycle
            for (int i = threshold; i < max; i = i + 2)
            {
                flag = false;
                for (int j = 0; j < basePrimes.Count; j++)
                {
                    if (i % basePrimes[j] == 0)
                    {
                        flag = true;
                        break;
                    }
                }
                // if current number wasn't divided by any already saved prime - it is a new prime
                if (!flag)
                {
                    result.Add(i);
                }
            }

            return result;
        }
        public static List<int> SieveOriginal_MT1(int max, int amountOfThreads)
        {
            // finding sqrt(n) threshold for single threaded algorithm
            int threshold = (int)Math.Ceiling(Math.Sqrt(max));
            threshold = threshold % 2 == 0 ? threshold + 1 : threshold;

            // finding list of primes below threshold value
            List<int> basePrimes = SieveOriginal(threshold);

            // result list is used by multiple threads, so ConcurrentBag is used
            ConcurrentBag<int> result = new ConcurrentBag<int>();

            Thread[] threads = new Thread[amountOfThreads];
            int step = (max - threshold) / amountOfThreads;

            for (int i = 0; i < amountOfThreads; i++)
            {
                if (i != amountOfThreads - 1)
                {
                    threads[i] = new Thread(SieveOriginal_MT1_threadFunc);
                    threads[i].Name = "Thread " + i;
                    threads[i].Start(new object[]
                    {
                        (object)(threshold + step * i),
                        (object)(threshold + step * ( i + 1 )),
                        (object)basePrimes,
                        (object)result
                    });
                }
                else
                {
                    // last thread gets different EndIndex value to avoid loosing last numbers
                    
                    threads[i] = new Thread(SieveOriginal_MT1_threadFunc);
                    threads[i].Name = "Thread " + i;
                    threads[i].Start(new object[]
                    {
                        (object)(threshold + step * i),
                        (object)max,
                        (object)basePrimes,
                        (object)result
                    });
                }
            }

            // waiting for all threads to finish working
            for (int i = 0; i < amountOfThreads; i++)
            {
                threads[i].Join();
            }

            basePrimes.AddRange(result.ToList<int>());
            return basePrimes;
        }
        public static List<int> SieveMasked_MT1(int max, int amountOfThreads)
        {
            // finding sqrt(n) threshold for single threaded algorithm
            int threshold = (int)Math.Ceiling(Math.Sqrt(max));
            threshold = threshold % 2 == 0 ? threshold + 1 : threshold;

            // finding list of primes below threshold value
            List<int> basePrimes = SieveMasked(threshold);

            // result list is used by multiple threads, so ConcurrentBag is used
            ConcurrentBag<int> result = new ConcurrentBag<int>();

            Thread[] threads = new Thread[amountOfThreads];
            int step = (max - threshold) / amountOfThreads;

            for (int i = 0; i < amountOfThreads; i++)
            {
                if (i != amountOfThreads - 1)
                {
                    threads[i] = new Thread(SieveOriginal_MT1_threadFunc);
                    threads[i].Name = "Thread " + i;
                    threads[i].Start(new object[]
                    {
                        (object)(threshold + step * i),
                        (object)(threshold + step * ( i + 1 )),
                        (object)basePrimes,
                        (object)result
                    });
                }
                else
                {
                    // last thread gets different EndIndex value to avoid loosing last numbers
                    threads[i] = new Thread(SieveOriginal_MT1_threadFunc);
                    threads[i].Name = "Thread " + i;
                    threads[i].Start(new object[]
                    {
                        (object)(threshold + step * i),
                        (object)max,
                        (object)basePrimes,
                        (object)result
                    });
                }
            }

            // waiting for all threads to finish working
            for (int i = 0; i < amountOfThreads; i++)
            {
                threads[i].Join();
            }

            basePrimes.AddRange(result.ToList<int>());
            return basePrimes;
        }
        public static List<int> SieveOriginal_MT2(int max, int amountOfThreads)
        {
            // finding sqrt(n) threshold for single threaded algorithm
            int threshold = (int)Math.Ceiling(Math.Sqrt(max));
            threshold = threshold % 2 == 0 ? threshold + 1 : threshold;

            Stopwatch watch = new Stopwatch();

            // finding list of primes below threshold value
            List<int> basePrimes = SieveOriginal(threshold);

            bool[] result = new bool[max];
            for (int i = 0; i < max; i++)
                result[i] = true;

            Thread[] threads = new Thread[amountOfThreads];
            // amount of base primes for each thread to check
            int step = basePrimes.Count / amountOfThreads;

            for (int i = 0; i < amountOfThreads; i++)
            {
                if (i != amountOfThreads - 1)
                {
                    threads[i] = new Thread(SieveOriginal_MT2_threadFunc);
                    threads[i].Name = "Thread " + i;
                    threads[i].Start(new object[]
                    {
                        (object)(int)Math.Ceiling((double)(step * i)),
                        (object)(int)Math.Floor((double)(step * ( i + 1 ))),
                        (object)basePrimes,
                        (object)result
                    });
                }
                else
                {
                    // last thread gets different EndIndex value to avoid loosing last numbers
                    threads[i] = new Thread(SieveOriginal_MT2_threadFunc);
                    threads[i].Name = "Thread " + i;
                    threads[i].Start(new object[]
                    {
                        (object)(int)Math.Ceiling((double)(step * i)),
                        (object)(basePrimes.Count - 1),
                        (object)basePrimes,
                        (object)result
                    });
                }
            }

            // waiting for all threads to finish working
            for (int i = 0; i < amountOfThreads; i++)
            {
                threads[i].Join();
            }

            // get the results
            for (int i = threshold; i < max; i++)
                if (result[i] == true)
                    basePrimes.Add(i);

            return basePrimes;
        }

        private static void SieveOriginal_MT1_threadFunc(object obj)
        {
            // PARAMETRES (int startIndex, int endIndex, List<int> basePrimes, result)
            // STARTINDEX NEEDS TO BE ODD
            object[] parametres = (object[])obj;
            int startIndex = (int)parametres[0] % 2 == 0 ? (int)parametres[0] - 1 : (int)parametres[0];
            int endIndex = (int)parametres[1];
            List<int> basePrimes = (List<int>)parametres[2];
            ConcurrentBag<int> result = (ConcurrentBag<int>)parametres[3];

            bool flag;

            for (int i = startIndex; i < endIndex; i = i + 2)
            {
                flag = false;
                for (int j = 0; j < basePrimes.Count; j++)
                {
                    if (i % basePrimes[j] == 0)
                    {
                        flag = true;
                        break;
                    }
                }
                // if current number wasn't divided by any already saved prime - it is a new prime
                if (!flag)
                {
                    result.Add(i);
                }
            }
        }
        //public static Object locker = new Object();
        private static void SieveOriginal_MT2_threadFunc(object obj)
        {
            // PARAMETRES (int startIndex, int endIndex, List<int> basePrimes, result)
            // STARTINDEX NEEDS TO BE ODD
            object[] parametres = (object[])obj;
            int startIndex = (int)parametres[0];
            int endIndex = (int)parametres[1];
            List<int> basePrimes = (List<int>)parametres[2];
            bool[] result = (bool[])parametres[3];

            int threshold = (int)Math.Ceiling(Math.Sqrt(result.Length));
            threshold = threshold % 2 == 0 ? threshold + 1 : threshold;

            for (int i = threshold; i < result.Length; i++)
            {
                if (result[i] == true)
                {
                    for (int j = startIndex; j <= endIndex; j++)
                    {
                        if (i % basePrimes[j] == 0)
                        {
                            //lock (locker)
                            //{
                                result[i] = false;
                            //}
                        }
                    }
                }
            }
        }
    }
}
